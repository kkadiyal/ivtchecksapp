package com.hughes.ivtchecks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IvtconfigurablechecksApplication {

	public static void main(String[] args) {
		SpringApplication.run(IvtconfigurablechecksApplication.class, args);
	}

}

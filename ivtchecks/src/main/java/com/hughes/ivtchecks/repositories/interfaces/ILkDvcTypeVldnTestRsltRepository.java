package com.hughes.ivtchecks.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestRslt;
import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestRsltPK;

@Repository
public interface ILkDvcTypeVldnTestRsltRepository extends
		JpaRepository<LkDvcTypeVldnTestRslt, LkDvcTypeVldnTestRsltPK> {
	List<LkDvcTypeVldnTestRslt> findAllByIdCompanyIdAndIdVldnTestRslt(
			String companyId, String vldnTestRslt);

	void deleteAllByIdCompanyIdAndIdDeviceCd(String companyId, String deviceCd);
}

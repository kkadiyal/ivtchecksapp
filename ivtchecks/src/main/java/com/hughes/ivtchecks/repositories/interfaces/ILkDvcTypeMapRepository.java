package com.hughes.ivtchecks.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hughes.ivtchecks.entities.LkDvcTypeMap;
import com.hughes.ivtchecks.entities.LkDvcTypeMapPK;

@Repository
public interface ILkDvcTypeMapRepository extends
		JpaRepository<LkDvcTypeMap, LkDvcTypeMapPK> {

	List<LkDvcTypeMap> findByDeviceCd(String deviceCd);
}

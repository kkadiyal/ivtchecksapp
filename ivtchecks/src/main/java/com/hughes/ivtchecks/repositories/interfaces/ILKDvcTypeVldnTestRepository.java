package com.hughes.ivtchecks.repositories.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hughes.ivtchecks.entities.LkDvcTypeVldnTest;
import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestPK;

@Repository
public interface ILKDvcTypeVldnTestRepository extends
		JpaRepository<LkDvcTypeVldnTest, LkDvcTypeVldnTestPK> {
	List<LkDvcTypeVldnTest> findAllByIdCompanyId(String companyId);

	void deleteAllByIdCompanyIdAndIdDeviceCd(String companyId, String deviceCd);

}

package com.hughes.ivtchecks.repositories.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hughes.ivtchecks.entities.NepCustomer;

@Repository
public interface INepCustomerRepository extends
		JpaRepository<NepCustomer, String> {

}

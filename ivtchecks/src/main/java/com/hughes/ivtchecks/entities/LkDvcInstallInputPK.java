package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the LK_DVC_INSTALL_INPUT database table.
 * 
 */
@Embeddable
public class LkDvcInstallInputPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "DEVICE_CD")
	private String deviceCd;

	@Column(name = "FLD_CD")
	private String fldCd;

	public LkDvcInstallInputPK() {
	}

	public String getDeviceCd() {
		return this.deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public String getFldCd() {
		return this.fldCd;
	}

	public void setFldCd(String fldCd) {
		this.fldCd = fldCd;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LkDvcInstallInputPK)) {
			return false;
		}
		LkDvcInstallInputPK castOther = (LkDvcInstallInputPK) other;
		return this.deviceCd.equals(castOther.deviceCd)
				&& this.fldCd.equals(castOther.fldCd);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.deviceCd.hashCode();
		hash = hash * prime + this.fldCd.hashCode();

		return hash;
	}
}
package com.hughes.ivtchecks.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the EIT_OVT_PERF_THRESHOLD database table.
 * 
 */
@Embeddable
public class EitOvtPerfThresholdPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="DEVICE_CD")
	private String deviceCd;

	private String emsid;

	@Column(name="PARAM_NAME")
	private String paramName;

	public EitOvtPerfThresholdPK() {
	}
	public String getDeviceCd() {
		return this.deviceCd;
	}
	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}
	public String getEmsid() {
		return this.emsid;
	}
	public void setEmsid(String emsid) {
		this.emsid = emsid;
	}
	public String getParamName() {
		return this.paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EitOvtPerfThresholdPK)) {
			return false;
		}
		EitOvtPerfThresholdPK castOther = (EitOvtPerfThresholdPK)other;
		return 
			this.deviceCd.equals(castOther.deviceCd)
			&& this.emsid.equals(castOther.emsid)
			&& this.paramName.equals(castOther.paramName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.deviceCd.hashCode();
		hash = hash * prime + this.emsid.hashCode();
		hash = hash * prime + this.paramName.hashCode();
		
		return hash;
	}
}
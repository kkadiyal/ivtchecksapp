package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the LK_DVC_TYPE_MAP database table.
 * 
 */
@Embeddable
public class LkDvcTypeMapPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "MAP_TYPE")
	private String mapType;

	@Column(name = "MAP_VALUE")
	private String mapValue;

	public LkDvcTypeMapPK() {
	}

	public String getMapType() {
		return this.mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	public String getMapValue() {
		return this.mapValue;
	}

	public void setMapValue(String mapValue) {
		this.mapValue = mapValue;
	}

}
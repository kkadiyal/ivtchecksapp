package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the LK_VLDN_TEST database table.
 * 
 */
@Entity
@Table(name = "LK_VLDN_TEST")
public class LkVldnTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "VLDN_TEST_CD")
	private String vldnTestCd;

	@Column(name = "VLDN_TEST_DESC")
	private String vldnTestDesc;

	@Column(name = "VLDN_TEST_TYPE")
	private String vldnTestType;

	public LkVldnTest() {
	}

	public String getVldnTestCd() {
		return this.vldnTestCd;
	}

	public void setVldnTestCd(String vldnTestCd) {
		this.vldnTestCd = vldnTestCd;
	}

	public String getVldnTestDesc() {
		return this.vldnTestDesc;
	}

	public void setVldnTestDesc(String vldnTestDesc) {
		this.vldnTestDesc = vldnTestDesc;
	}

	public String getVldnTestType() {
		return this.vldnTestType;
	}

	public void setVldnTestType(String vldnTestType) {
		this.vldnTestType = vldnTestType;
	}

}
package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the LK_DVC_INSTALL_INPUT database table.
 * 
 */
@Entity
@Table(name = "LK_DVC_INSTALL_INPUT")
public class LkDvcInstallInput implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LkDvcInstallInputPK id;

	public LkDvcInstallInput() {
	}

	public LkDvcInstallInputPK getId() {
		return id;
	}

	public void setId(LkDvcInstallInputPK id) {
		this.id = id;
	}

}
package com.hughes.ivtchecks.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the LK_DVC_TYPE_VLDN_TEST database table.
 * 
 */
@Entity
@Table(name = "LK_DVC_TYPE_VLDN_TEST")
public class LkDvcTypeVldnTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LkDvcTypeVldnTestPK id;

	@Column(name = "ACTIVE_FL")
	private String activeFl;

	private BigDecimal ordr;

	public LkDvcTypeVldnTest() {
	}

	public LkDvcTypeVldnTestPK getId() {
		return this.id;
	}

	public void setId(LkDvcTypeVldnTestPK id) {
		this.id = id;
	}

	public String getActiveFl() {
		return this.activeFl;
	}

	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}

	public BigDecimal getOrdr() {
		return this.ordr;
	}

	public void setOrdr(BigDecimal ordr) {
		this.ordr = ordr;
	}

}
package com.hughes.ivtchecks.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the EIT_OVT_PERF_THRESHOLD database table.
 * 
 */
@Entity
@Table(name = "EIT_OVT_PERF_THRESHOLD")
public class EitOvtPerfThreshold implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EitOvtPerfThresholdPK id;

	@Column(name = "IS_ENABLED")
	private BigDecimal isEnabled;

	@Column(name = "MAX_THRESHOLD")
	private BigDecimal maxThreshold;

	@Column(name = "MIN_THRESHOLD")
	private BigDecimal minThreshold;

	@Column(name = "PARAM_DESC")
	private String paramDesc;

	@Column(name = "PARAM_TYPE")
	private String paramType;

	@Column(name = "PCT_THRESHOLD")
	private BigDecimal pctThreshold;

	@Temporal(TemporalType.DATE)
	@Column(name = "UPDATED_DTTM")
	private Date updatedDttm;

	public EitOvtPerfThreshold() {
	}

	public EitOvtPerfThresholdPK getId() {
		return this.id;
	}

	public void setId(EitOvtPerfThresholdPK id) {
		this.id = id;
	}

	public BigDecimal getIsEnabled() {
		return this.isEnabled;
	}

	public void setIsEnabled(BigDecimal isEnabled) {
		this.isEnabled = isEnabled;
	}

	public BigDecimal getMaxThreshold() {
		return this.maxThreshold;
	}

	public void setMaxThreshold(BigDecimal maxThreshold) {
		this.maxThreshold = maxThreshold;
	}

	public BigDecimal getMinThreshold() {
		return this.minThreshold;
	}

	public void setMinThreshold(BigDecimal minThreshold) {
		this.minThreshold = minThreshold;
	}

	public String getParamDesc() {
		return this.paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}

	public String getParamType() {
		return this.paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}

	public BigDecimal getPctThreshold() {
		return this.pctThreshold;
	}

	public void setPctThreshold(BigDecimal pctThreshold) {
		this.pctThreshold = pctThreshold;
	}

	public Date getUpdatedDttm() {
		return this.updatedDttm;
	}

	public void setUpdatedDttm(Date updatedDttm) {
		this.updatedDttm = updatedDttm;
	}

}
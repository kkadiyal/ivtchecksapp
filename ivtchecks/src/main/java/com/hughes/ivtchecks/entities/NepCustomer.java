package com.hughes.ivtchecks.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the NEP_CUSTOMERS database table.
 * 
 */
@Entity
@Table(name="NEP_CUSTOMERS")
public class NepCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HNS_COMPANY_ID")
	private String hnsCompanyId;

	@Column(name="HNSPARENTCOMPANY_ID")
	private String hnsparentcompanyId;

	public NepCustomer() {
	}

	public String getHnsCompanyId() {
		return this.hnsCompanyId;
	}

	public void setHnsCompanyId(String hnsCompanyId) {
		this.hnsCompanyId = hnsCompanyId;
	}

	public String getHnsparentcompanyId() {
		return this.hnsparentcompanyId;
	}

	public void setHnsparentcompanyId(String hnsparentcompanyId) {
		this.hnsparentcompanyId = hnsparentcompanyId;
	}

}
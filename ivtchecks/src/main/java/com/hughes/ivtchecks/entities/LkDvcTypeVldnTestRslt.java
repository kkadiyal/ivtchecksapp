package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the LK_DVC_TYPE_VLDN_TEST_RSLT database table.
 * 
 */
@Entity
@Table(name = "LK_DVC_TYPE_VLDN_TEST_RSLT")
@NamedQuery(name = "LkDvcTypeVldnTestRslt.findAll", query = "SELECT l FROM LkDvcTypeVldnTestRslt l")
public class LkDvcTypeVldnTestRslt implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LkDvcTypeVldnTestRsltPK id;

	@Column(name = "ASSET_STATUS")
	private String assetStatus;

	@Column(name = "OPERATIONAL_STATUS")
	private String operationalStatus;

	public LkDvcTypeVldnTestRslt() {
	}

	public LkDvcTypeVldnTestRsltPK getId() {
		return this.id;
	}

	public void setId(LkDvcTypeVldnTestRsltPK id) {
		this.id = id;
	}

	public String getAssetStatus() {
		return this.assetStatus;
	}

	public void setAssetStatus(String assetStatus) {
		this.assetStatus = assetStatus;
	}

	public String getOperationalStatus() {
		return this.operationalStatus;
	}

	public void setOperationalStatus(String operationalStatus) {
		this.operationalStatus = operationalStatus;
	}

}
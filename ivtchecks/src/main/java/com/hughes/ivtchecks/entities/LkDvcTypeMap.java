package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The persistent class for the LK_DVC_TYPE_MAP database table.
 * 
 */
@Entity
@Table(name = "LK_DVC_TYPE_MAP")
public class LkDvcTypeMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private LkDvcTypeMapPK id;

	@Column(name = "DEVICE_CD")
	private String deviceCd;

	public LkDvcTypeMap() {
	}

	public LkDvcTypeMapPK getId() {
		return this.id;
	}

	public void setId(LkDvcTypeMapPK id) {
		this.id = id;
	}

	public String getDeviceCd() {
		return this.deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}
}
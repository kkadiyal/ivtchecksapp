package com.hughes.ivtchecks.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the LK_DVC_TYPE_VLDN_TEST database table.
 * 
 */
@Embeddable
public class LkDvcTypeVldnTestPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="DEVICE_CD")
	private String deviceCd;

	@Column(name="VLDN_TEST_CD")
	private String vldnTestCd;

	@Column(name="COMPANY_ID")
	private String companyId;

	public LkDvcTypeVldnTestPK() {
	}
	public String getDeviceCd() {
		return this.deviceCd;
	}
	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}
	public String getVldnTestCd() {
		return this.vldnTestCd;
	}
	public void setVldnTestCd(String vldnTestCd) {
		this.vldnTestCd = vldnTestCd;
	}
	public String getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof LkDvcTypeVldnTestPK)) {
			return false;
		}
		LkDvcTypeVldnTestPK castOther = (LkDvcTypeVldnTestPK)other;
		return 
			this.deviceCd.equals(castOther.deviceCd)
			&& this.vldnTestCd.equals(castOther.vldnTestCd)
			&& this.companyId.equals(castOther.companyId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.deviceCd.hashCode();
		hash = hash * prime + this.vldnTestCd.hashCode();
		hash = hash * prime + this.companyId.hashCode();
		
		return hash;
	}
}
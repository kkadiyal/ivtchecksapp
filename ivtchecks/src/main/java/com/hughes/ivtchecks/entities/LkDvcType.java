package com.hughes.ivtchecks.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the LK_DVC_TYPE database table.
 * 
 */
@Entity
@Table(name = "LK_DVC_TYPE")
public class LkDvcType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DEVICE_CD")
	private String deviceCd;

	@Column(name = "DEVICE_DESC")
	private String deviceDesc;

	public LkDvcType() {
	}

	public String getDeviceCd() {
		return this.deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public String getDeviceDesc() {
		return this.deviceDesc;
	}

	public void setDeviceDesc(String deviceDesc) {
		this.deviceDesc = deviceDesc;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((deviceCd == null) ? 0 : deviceCd.hashCode());
		result = prime * result
				+ ((deviceDesc == null) ? 0 : deviceDesc.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LkDvcType other = (LkDvcType) obj;
		if (deviceCd == null) {
			if (other.deviceCd != null)
				return false;
		} else if (!deviceCd.equals(other.deviceCd))
			return false;
		if (deviceDesc == null) {
			if (other.deviceDesc != null)
				return false;
		} else if (!deviceDesc.equals(other.deviceDesc))
			return false;
		return true;
	}
}
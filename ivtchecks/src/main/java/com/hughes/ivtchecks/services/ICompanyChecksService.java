package com.hughes.ivtchecks.services;

import com.hughes.ivtchecks.model.CompanyIdChecksRequest;
import com.hughes.ivtchecks.model.CompanyIdChecksResponse;
import com.hughes.ivtchecks.model.ResetToDefaultRequest;
import com.hughes.ivtchecks.model.CommonResponseObj;
import com.hughes.ivtchecks.model.UpdateChecksRequest;

public interface ICompanyChecksService {
	CompanyIdChecksResponse getConfiguredChecksForRequest(
			CompanyIdChecksRequest checksRequest);

	CommonResponseObj resetToDefaultForDeviceCd(
			ResetToDefaultRequest resetRequest);

	CommonResponseObj updateForDeviceCdByCustomer(
			UpdateChecksRequest updateRequest);
}

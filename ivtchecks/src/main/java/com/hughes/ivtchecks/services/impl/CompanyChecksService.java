package com.hughes.ivtchecks.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hughes.ivtchecks.entities.LkDvcTypeVldnTest;
import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestPK;
import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestRslt;
import com.hughes.ivtchecks.entities.LkDvcTypeVldnTestRsltPK;
import com.hughes.ivtchecks.model.CommonResponseObj;
import com.hughes.ivtchecks.model.CompanyIdChecksRequest;
import com.hughes.ivtchecks.model.CompanyIdChecksResponse;
import com.hughes.ivtchecks.model.Modelkey;
import com.hughes.ivtchecks.model.ResetToDefaultRequest;
import com.hughes.ivtchecks.model.UiApiModel;
import com.hughes.ivtchecks.model.UpdateChecksRequest;
import com.hughes.ivtchecks.model.UpdateDeviceModelObj;
import com.hughes.ivtchecks.repositories.interfaces.ILKDvcTypeVldnTestRepository;
import com.hughes.ivtchecks.repositories.interfaces.ILkDvcTypeVldnTestRsltRepository;
import com.hughes.ivtchecks.services.ICompanyChecksService;

@Service
public class CompanyChecksService implements ICompanyChecksService {

	@Autowired
	private ILKDvcTypeVldnTestRepository lKDvcTypeVldnTestRepository;

	@Autowired
	private ILkDvcTypeVldnTestRsltRepository lkDvcTypeVldnTestRsltRepository;

	@Override
	public CompanyIdChecksResponse getConfiguredChecksForRequest(
			CompanyIdChecksRequest checksRequest) {
		CompanyIdChecksResponse responseFOrCompanyIdChecksReq = new CompanyIdChecksResponse();
		if (checksRequest == null || checksRequest.getCompanyId() == null
				|| checksRequest.getCompanyId().isEmpty()) {
			responseFOrCompanyIdChecksReq.setResponseCode("REQ Error");
			responseFOrCompanyIdChecksReq
					.setResponseDesc("Request is null or Empty");
			return responseFOrCompanyIdChecksReq;
		}
		String companyId = checksRequest.getCompanyId();
		Map<String, List<UiApiModel>> map = null;
		if (companyId.equals("DEFAULT")) {
			List<LkDvcTypeVldnTest> listOfVldnChecks = lKDvcTypeVldnTestRepository
					.findAllByIdCompanyId(companyId);
			List<LkDvcTypeVldnTestRslt> listOfVldnChecksRslt = lkDvcTypeVldnTestRsltRepository
					.findAllByIdCompanyIdAndIdVldnTestRslt(companyId, "FAIL");
			Map<Modelkey, LkDvcTypeVldnTest> modelToTestMap = new HashMap<Modelkey, LkDvcTypeVldnTest>();
			Map<Modelkey, LkDvcTypeVldnTestRslt> modelToResultMap = new HashMap<Modelkey, LkDvcTypeVldnTestRslt>();
			makeMapFromTests(modelToTestMap, listOfVldnChecks);
			makeMapFromTestRslts(modelToResultMap, listOfVldnChecksRslt);
			map = parseIntoMapByDeviceid(modelToTestMap, modelToResultMap);
			// map = processAndSetDefaultsMap(companyId);
		} else {
			List<LkDvcTypeVldnTest> listOfVldnChecks = lKDvcTypeVldnTestRepository
					.findAllByIdCompanyId("DEFAULT");
			List<LkDvcTypeVldnTestRslt> listOfVldnChecksRslt = lkDvcTypeVldnTestRsltRepository
					.findAllByIdCompanyIdAndIdVldnTestRslt("DEFAULT", "FAIL");

			Map<Modelkey, LkDvcTypeVldnTest> modelToTestMap = new HashMap<Modelkey, LkDvcTypeVldnTest>();
			Map<Modelkey, LkDvcTypeVldnTestRslt> modelToResultMap = new HashMap<Modelkey, LkDvcTypeVldnTestRslt>();
			makeMapFromTests(modelToTestMap, listOfVldnChecks);
			makeMapFromTestRslts(modelToResultMap, listOfVldnChecksRslt);

			map = parseIntoMapByDeviceid(modelToTestMap, modelToResultMap);

			// map = processAndSetDefaultsMap("DEFAULT");
			processForSpecificCustomerAndAddToMap(companyId, map,
					modelToTestMap, modelToResultMap);
		}
		responseFOrCompanyIdChecksReq.setMap(map);
		responseFOrCompanyIdChecksReq.setResponseCode("SUCCESS");
		responseFOrCompanyIdChecksReq.setResponseDesc("Able to fetch results");
		return responseFOrCompanyIdChecksReq;
	}

	private void processForSpecificCustomerAndAddToMap(String companyId,
			Map<String, List<UiApiModel>> deviceCdToMUiModelMapForChecks,
			Map<Modelkey, LkDvcTypeVldnTest> modelToTestMap,
			Map<Modelkey, LkDvcTypeVldnTestRslt> modelToResultMap) {
		List<LkDvcTypeVldnTest> listOfVldnChecks = lKDvcTypeVldnTestRepository
				.findAllByIdCompanyId(companyId);
		List<LkDvcTypeVldnTestRslt> listOfVldnChecksRslt = lkDvcTypeVldnTestRsltRepository
				.findAllByIdCompanyIdAndIdVldnTestRslt(companyId, "FAIL");
		Map<Modelkey, LkDvcTypeVldnTest> modelToTestMapForSpecificCustomer = new HashMap<Modelkey, LkDvcTypeVldnTest>();
		Map<Modelkey, LkDvcTypeVldnTestRslt> modelToResultMapForSpecificCustomer = new HashMap<Modelkey, LkDvcTypeVldnTestRslt>();
		makeMapFromTests(modelToTestMapForSpecificCustomer, listOfVldnChecks);
		makeMapFromTestRslts(modelToResultMapForSpecificCustomer,
				listOfVldnChecksRslt);

		Set<Modelkey> set = modelToTestMapForSpecificCustomer.keySet();
		for (Modelkey setItr : set) {
			LkDvcTypeVldnTest testItr = modelToTestMapForSpecificCustomer
					.get(setItr);

			List<UiApiModel> uiModelsList = deviceCdToMUiModelMapForChecks
					.get(setItr.getDeviceCd());

			for (UiApiModel modelItr : uiModelsList) {
				if (modelItr.getDeviceCd()
						.equals(testItr.getId().getDeviceCd())
						&& modelItr.getCheckName().equals(
								testItr.getId().getVldnTestCd())) {
					if (!modelItr.getCheckStatus()
							.equals(testItr.getActiveFl())) {
						modelItr.setCheckStatus(testItr.getActiveFl());
						modelItr.setCheckSpecific("2");

					}
				}
				modelItr.setCompanyName(companyId);
			}
		}

		Set<Modelkey> set1 = modelToResultMapForSpecificCustomer.keySet();
		for (Modelkey setItr : set1) {
			LkDvcTypeVldnTestRslt rsltItr = modelToResultMapForSpecificCustomer
					.get(setItr);

			List<UiApiModel> uiModelsList = deviceCdToMUiModelMapForChecks
					.get(setItr.getDeviceCd());

			for (UiApiModel modelItr : uiModelsList) {
				if (modelItr.getDeviceCd()
						.equals(rsltItr.getId().getDeviceCd())
						&& modelItr.getCheckName().equals(
								rsltItr.getId().getVldnTestCd())) {
					if (!modelItr.getFailureResult().equals(
							rsltItr.getAssetStatus())) {
						modelItr.setFailureResult(rsltItr.getAssetStatus());
						if (modelItr.getCheckSpecific().equals("1"))
							modelItr.setCheckSpecific("3");
						else if (modelItr.getCheckSpecific().equals("2"))
							modelItr.setCheckSpecific("4");
					}
				}
				modelItr.setCompanyName(companyId);
			}

		}

	}

	/*
	 * private Map<String, List<UiApiModel>> processAndSetDefaultsMap( String
	 * companyId) { List<LkDvcTypeVldnTest> listOfVldnChecks =
	 * lKDvcTypeVldnTestRepository .findAllByIdCompanyId(companyId);
	 * List<LkDvcTypeVldnTestRslt> listOfVldnChecksRslt =
	 * lkDvcTypeVldnTestRsltRepository
	 * .findAllByIdCompanyIdAndIdVldnTestRslt(companyId, "FAIL"); Map<String,
	 * List<UiApiModel>> map = parseIntoMapByDeviceid( listOfVldnChecks,
	 * listOfVldnChecksRslt); return map; }
	 */

	private Map<String, List<UiApiModel>> parseIntoMapByDeviceid(
			Map<Modelkey, LkDvcTypeVldnTest> modelToTestMap,
			Map<Modelkey, LkDvcTypeVldnTestRslt> modelToResultMap) {

		// System.out.println(map1);
		// System.out.println(map2);
		Map<String, List<UiApiModel>> deviceCdToMUiModelMapForChecks = new HashMap<String, List<UiApiModel>>();
		Set<Modelkey> set = modelToTestMap.keySet();
		for (Modelkey setItr : set) {
			LkDvcTypeVldnTest testItr = modelToTestMap.get(setItr);
			LkDvcTypeVldnTestRslt rsltItr = modelToResultMap.get(setItr);
			// System.out.println(setItr);
			if (deviceCdToMUiModelMapForChecks.get(setItr.getDeviceCd()) == null) {
				List<UiApiModel> list = new ArrayList<UiApiModel>();
				UiApiModel modelObj = new UiApiModel();
				modelObj.setDeviceCd(testItr.getId().getDeviceCd());
				modelObj.setCheckName(testItr.getId().getVldnTestCd());
				modelObj.setCheckStatus(testItr.getActiveFl());
				modelObj.setCheckSpecific("1");
				modelObj.setCompanyName(testItr.getId().getCompanyId());
				modelObj.setFailureResult(rsltItr.getAssetStatus());
				list.add(modelObj);
				deviceCdToMUiModelMapForChecks.put(testItr.getId()
						.getDeviceCd(), list);
			} else {
				UiApiModel modelObj = new UiApiModel();
				modelObj.setDeviceCd(testItr.getId().getDeviceCd());
				modelObj.setCheckName(testItr.getId().getVldnTestCd());
				modelObj.setCheckStatus(testItr.getActiveFl());
				modelObj.setCheckSpecific("1");
				modelObj.setCompanyName(testItr.getId().getCompanyId());
				modelObj.setFailureResult(rsltItr.getAssetStatus());
				deviceCdToMUiModelMapForChecks.get(
						testItr.getId().getDeviceCd()).add(modelObj);
			}

		}
		return deviceCdToMUiModelMapForChecks;
	}

	private void makeMapFromTests(Map<Modelkey, LkDvcTypeVldnTest> map1,
			List<LkDvcTypeVldnTest> listOfVldnChecks) {
		if (listOfVldnChecks != null && listOfVldnChecks.size() > 0) {
			for (LkDvcTypeVldnTest testItr : listOfVldnChecks) {
				Modelkey key = new Modelkey();
				key.setDeviceCd(testItr.getId().getDeviceCd());
				key.setCheckName(testItr.getId().getVldnTestCd());
				map1.put(key, testItr);
			}
		}
	}

	private void makeMapFromTestRslts(
			Map<Modelkey, LkDvcTypeVldnTestRslt> map1,
			List<LkDvcTypeVldnTestRslt> listOfVldnRsltChecks) {
		if (listOfVldnRsltChecks != null && listOfVldnRsltChecks.size() > 0) {
			for (LkDvcTypeVldnTestRslt testItr : listOfVldnRsltChecks) {
				Modelkey key = new Modelkey();
				key.setDeviceCd(testItr.getId().getDeviceCd());
				key.setCheckName(testItr.getId().getVldnTestCd());
				map1.put(key, testItr);
			}
		}
	}

	@Override
	@Transactional
	public CommonResponseObj resetToDefaultForDeviceCd(
			ResetToDefaultRequest resetRequest) {
		CommonResponseObj response = new CommonResponseObj();
		if (resetRequest == null || resetRequest.getCompanyId() == null
				|| resetRequest.getDeviceCd() == null
				|| resetRequest.getCompanyId().isEmpty()
				|| resetRequest.getDeviceCd().isEmpty()) {
			response.setResponseCode("FAILURE");
			response.setResponseMsg("Request is null or empty");
			return response;
		}
		if (resetRequest.getCompanyId().equals("DEFAULT")) {
			response.setResponseCode("FAILURE");
			response.setResponseMsg("Request Cannot be performed on DEFAULT");
			return response;
		}
		deleteFromTestsAndResults(resetRequest.getCompanyId(),
				resetRequest.getDeviceCd());
		response.setResponseCode("SUCCESS");
		response.setResponseMsg("Request Performed Successfully");

		return response;
	}

	private void deleteFromTestsAndResults(String customerId, String deviceCd) {
		lKDvcTypeVldnTestRepository.deleteAllByIdCompanyIdAndIdDeviceCd(
				customerId, deviceCd);
		lkDvcTypeVldnTestRsltRepository.deleteAllByIdCompanyIdAndIdDeviceCd(
				customerId, deviceCd);

	}

	@Override
	@Transactional
	public CommonResponseObj updateForDeviceCdByCustomer(
			UpdateChecksRequest updateRequest) {
		CommonResponseObj response = new CommonResponseObj();
		if (updateRequest == null || updateRequest.getCompanyId() == null
				|| updateRequest.getDeviceCd() == null
				|| updateRequest.getDeviceList() == null
				|| updateRequest.getCompanyId().isEmpty()
				|| updateRequest.getDeviceCd().isEmpty()
				|| updateRequest.getDeviceList().isEmpty()) {
			response.setResponseCode("FAILURE");
			response.setResponseMsg("Request is null or empty");
			return response;
		}
		if (updateRequest.getDeviceList().size() == 0) {
			response.setResponseCode("FAILURE");
			response.setResponseMsg("Request checks are empty");
			return response;
		}
		if (updateRequest.getCompanyId().equals("DEFAULT")) {
			response.setResponseCode("FAILURE");
			response.setResponseMsg("Not Allowed to update default checks");
			return response;
		}
		List<LkDvcTypeVldnTest> vldnTestlist = new ArrayList<LkDvcTypeVldnTest>();
		List<LkDvcTypeVldnTestRslt> vldnTestRsltList = new ArrayList<LkDvcTypeVldnTestRslt>();
		createRecordsFromUpdateRequest(updateRequest, vldnTestlist,
				vldnTestRsltList);
		deleteFromTestsAndResults(updateRequest.getCompanyId(),
				updateRequest.getDeviceCd());
		lKDvcTypeVldnTestRepository.saveAll(vldnTestlist);
		lkDvcTypeVldnTestRsltRepository.saveAll(vldnTestRsltList);
		;
		response.setResponseCode("SUCCESS");
		response.setResponseMsg("Done Updating for "
				+ updateRequest.getCompanyId() + " device "
				+ updateRequest.getDeviceCd());
		return response;
	}

	private void createRecordsFromUpdateRequest(
			UpdateChecksRequest updateRequest,
			List<LkDvcTypeVldnTest> vldnTestlist,
			List<LkDvcTypeVldnTestRslt> vldnTestRsltList) {
		List<UpdateDeviceModelObj> updateList = updateRequest.getDeviceList();
		for (UpdateDeviceModelObj modelItr : updateList) {
			LkDvcTypeVldnTest testRec = getVldnTestRecord(
					updateRequest.getCompanyId(), updateRequest.getDeviceCd(),
					modelItr.getCheckName(), modelItr.getCheckStatus());
			vldnTestlist.add(testRec);
			LkDvcTypeVldnTestRslt resultForFail = getVldnTestResultRecord(
					updateRequest.getCompanyId(), updateRequest.getDeviceCd(),
					modelItr.getCheckName(), "FAIL",
					modelItr.getFailureResult());
			LkDvcTypeVldnTestRslt resultForPass = getVldnTestResultRecord(
					updateRequest.getCompanyId(), updateRequest.getDeviceCd(),
					modelItr.getCheckName(), "PASS", "PASS");
			vldnTestRsltList.add(resultForPass);
			vldnTestRsltList.add(resultForFail);
		}
	}

	private LkDvcTypeVldnTestRslt getVldnTestResultRecord(String customerId,
			String deviceCd, String checkName, String checkResult,
			String assetResultCode) {
		LkDvcTypeVldnTestRsltPK vldnTestRsltPk = new LkDvcTypeVldnTestRsltPK();
		vldnTestRsltPk.setCompanyId(customerId);
		vldnTestRsltPk.setDeviceCd(deviceCd);
		vldnTestRsltPk.setVldnTestCd(checkName);
		vldnTestRsltPk.setVldnTestRslt(checkResult);
		LkDvcTypeVldnTestRslt vldnTestRslt = new LkDvcTypeVldnTestRslt();
		vldnTestRslt.setId(vldnTestRsltPk);
		vldnTestRslt.setAssetStatus(assetResultCode);
		vldnTestRslt.setOperationalStatus("Normal");
		return vldnTestRslt;
	}

	private LkDvcTypeVldnTest getVldnTestRecord(String customerId,
			String deviceCd, String checkName, String checkStatus) {
		LkDvcTypeVldnTest testRec = new LkDvcTypeVldnTest();
		LkDvcTypeVldnTestPK testRecPK = new LkDvcTypeVldnTestPK();
		testRecPK.setCompanyId(customerId);
		testRecPK.setDeviceCd(deviceCd);
		testRecPK.setVldnTestCd(checkName);
		testRec.setId(testRecPK);
		testRec.setActiveFl(checkStatus);
		return testRec;
	}
}

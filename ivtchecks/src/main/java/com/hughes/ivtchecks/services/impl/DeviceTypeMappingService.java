package com.hughes.ivtchecks.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hughes.ivtchecks.entities.LkDvcTypeMap;
import com.hughes.ivtchecks.model.DeviceCdMappingsRequest;
import com.hughes.ivtchecks.model.DeviceMappingObject;
import com.hughes.ivtchecks.model.ResponseObjForDeviceMapping;
import com.hughes.ivtchecks.repositories.interfaces.ILkDvcTypeMapRepository;
import com.hughes.ivtchecks.services.IDeviceTypeMappingService;

@Service
public class DeviceTypeMappingService implements IDeviceTypeMappingService {

	@Autowired
	private ILkDvcTypeMapRepository lkDvcTypeMapRepository;

	@Override
	public ResponseObjForDeviceMapping getAllDeviceTypeMappingsMasterList() {
		List<LkDvcTypeMap> listOfDeviceMap = lkDvcTypeMapRepository.findAll();
		List<DeviceMappingObject> mappingList = new ArrayList<DeviceMappingObject>();
		ResponseObjForDeviceMapping resp = new ResponseObjForDeviceMapping();
		if (listOfDeviceMap != null && listOfDeviceMap.size() > 0) {
			for (LkDvcTypeMap deviceTypeItr : listOfDeviceMap) {
				DeviceMappingObject mapObj = new DeviceMappingObject();
				if (deviceTypeItr.getDeviceCd() != null)
					mapObj.setDeviceCd(deviceTypeItr.getDeviceCd());
				if (deviceTypeItr.getId().getMapType() != null)
					mapObj.setDeviceMapType(deviceTypeItr.getId().getMapType());
				if (deviceTypeItr.getId().getMapValue() != null)
					mapObj.setDeviceMapValue(deviceTypeItr.getId()
							.getMapValue());
				mappingList.add(mapObj);
			}
			resp.setResponseCode("SUCCESS");
			resp.setResponseDesc("Mappings are as follows");
		} else {
			resp.setResponseCode("NOT FOUND");
			resp.setResponseDesc("Mappings are empty in DB");
		}
		resp.setDeviceCdMappingList(mappingList);
		return resp;
	}

	@Override
	public ResponseObjForDeviceMapping getAllDeviceTypeMappingsForDeviceCd(
			DeviceCdMappingsRequest request) {
		ResponseObjForDeviceMapping resp = new ResponseObjForDeviceMapping();
		if (request == null || request.getDeviceCd() == null ||request.getDeviceCd().isEmpty()) {
			resp.setResponseCode("REQ Error");
			resp.setResponseDesc("Request is null or Empty");
			return resp;
		}
		List<LkDvcTypeMap> listOfDeviceMap = lkDvcTypeMapRepository
				.findByDeviceCd(request.getDeviceCd());
		List<DeviceMappingObject> mappingList = new ArrayList<DeviceMappingObject>();
		if (listOfDeviceMap != null && listOfDeviceMap.size() > 0) {
			for (LkDvcTypeMap deviceTypeItr : listOfDeviceMap) {
				DeviceMappingObject mapObj = new DeviceMappingObject();
				if (deviceTypeItr.getDeviceCd() != null)
					mapObj.setDeviceCd(deviceTypeItr.getDeviceCd());
				if (deviceTypeItr.getId().getMapType() != null)
					mapObj.setDeviceMapType(deviceTypeItr.getId().getMapType());
				if (deviceTypeItr.getId().getMapValue() != null)
					mapObj.setDeviceMapValue(deviceTypeItr.getId()
							.getMapValue());
				mappingList.add(mapObj);
			}
			resp.setResponseCode("SUCCESS");
			resp.setResponseDesc("Mappings are as follows");
		} else {
			resp.setResponseCode("NOT FOUND");
			resp.setResponseDesc("Mappings are empty in DB");
		}
		resp.setDeviceCdMappingList(mappingList);
		return resp;
	}

}

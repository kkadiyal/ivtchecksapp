package com.hughes.ivtchecks.services;

import com.hughes.ivtchecks.model.CompaniesListResult;

public interface ICompaniesMasterListService {

	CompaniesListResult getMasterListOfComapaniesWithNEPEnabled();
}

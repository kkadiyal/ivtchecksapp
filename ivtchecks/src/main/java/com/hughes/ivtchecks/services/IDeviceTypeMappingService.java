package com.hughes.ivtchecks.services;

import com.hughes.ivtchecks.model.DeviceCdMappingsRequest;
import com.hughes.ivtchecks.model.ResponseObjForDeviceMapping;

public interface IDeviceTypeMappingService {
	ResponseObjForDeviceMapping getAllDeviceTypeMappingsMasterList();

	ResponseObjForDeviceMapping getAllDeviceTypeMappingsForDeviceCd(
			DeviceCdMappingsRequest request);
}

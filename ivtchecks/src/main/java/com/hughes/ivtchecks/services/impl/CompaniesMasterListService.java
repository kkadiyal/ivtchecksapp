package com.hughes.ivtchecks.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hughes.ivtchecks.entities.NepCustomer;
import com.hughes.ivtchecks.model.CompaniesListResult;
import com.hughes.ivtchecks.repositories.interfaces.INepCustomerRepository;
import com.hughes.ivtchecks.services.ICompaniesMasterListService;

@Service
public class CompaniesMasterListService implements ICompaniesMasterListService {

	@Autowired
	private INepCustomerRepository nepCustomerRepository;

	@Override
	public CompaniesListResult getMasterListOfComapaniesWithNEPEnabled() {
		List<NepCustomer> nepCustomersList = nepCustomerRepository.findAll();
		//nepCustomersList = nepCustomerRepository.findAllOrderByHnsCompanyId();
		CompaniesListResult companyListResultObject = new CompaniesListResult();
		companyListResultObject.setNepCustomers(nepCustomersList);
		return companyListResultObject;
	}
}

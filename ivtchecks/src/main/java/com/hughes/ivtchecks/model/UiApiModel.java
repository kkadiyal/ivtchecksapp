package com.hughes.ivtchecks.model;

public class UiApiModel {
	private String deviceCd;
	private String checkName;
	private String checkStatus;
	private String failureResult;
	private String checkSpecific;
	private String companyName;

	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getFailureResult() {
		return failureResult;
	}

	public void setFailureResult(String failureResult) {
		this.failureResult = failureResult;
	}

	public String getCheckSpecific() {
		return checkSpecific;
	}

	public void setCheckSpecific(String checkSpecific) {
		this.checkSpecific = checkSpecific;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((checkName == null) ? 0 : checkName.hashCode());
		result = prime * result
				+ ((checkSpecific == null) ? 0 : checkSpecific.hashCode());
		result = prime * result
				+ ((checkStatus == null) ? 0 : checkStatus.hashCode());
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result
				+ ((deviceCd == null) ? 0 : deviceCd.hashCode());
		result = prime * result
				+ ((failureResult == null) ? 0 : failureResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UiApiModel other = (UiApiModel) obj;
		if (checkName == null) {
			if (other.checkName != null)
				return false;
		} else if (!checkName.equals(other.checkName))
			return false;

		if (deviceCd == null) {
			if (other.deviceCd != null)
				return false;
		} else if (!deviceCd.equals(other.deviceCd))
			return false;

		return true;
	}
}

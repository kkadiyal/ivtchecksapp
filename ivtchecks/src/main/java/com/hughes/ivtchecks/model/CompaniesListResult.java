package com.hughes.ivtchecks.model;

import java.util.List;

import com.hughes.ivtchecks.entities.NepCustomer;

public class CompaniesListResult {

	private List<NepCustomer> nepCustomers;

	public List<NepCustomer> getNepCustomers() {
		return nepCustomers;
	}

	public void setNepCustomers(List<NepCustomer> nepCustomers) {
		this.nepCustomers = nepCustomers;
	}
}

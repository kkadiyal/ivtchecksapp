package com.hughes.ivtchecks.model;

public class Modelkey {

	private String deviceCd;
	private String checkName;

	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	@Override
	public String toString() {
		return deviceCd + ":" + checkName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((checkName == null) ? 0 : checkName.hashCode());
		result = prime * result
				+ ((deviceCd == null) ? 0 : deviceCd.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Modelkey other = (Modelkey) obj;
		if (checkName == null) {
			if (other.checkName != null)
				return false;
		} else if (!checkName.equals(other.checkName))
			return false;
		if (deviceCd == null) {
			if (other.deviceCd != null)
				return false;
		} else if (!deviceCd.equals(other.deviceCd))
			return false;
		return true;
	}

}

package com.hughes.ivtchecks.model;

public class RequestCompanyObject {

	private String companyId;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
}

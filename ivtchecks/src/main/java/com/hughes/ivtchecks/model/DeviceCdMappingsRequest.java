package com.hughes.ivtchecks.model;

public class DeviceCdMappingsRequest {
	private String deviceCd;

	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}
}

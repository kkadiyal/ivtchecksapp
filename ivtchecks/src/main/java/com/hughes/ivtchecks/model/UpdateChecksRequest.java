package com.hughes.ivtchecks.model;

import java.util.List;

public class UpdateChecksRequest {
	private String companyId;
	private String deviceCd;
	private List<UpdateDeviceModelObj> deviceList;



	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public List<UpdateDeviceModelObj> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(List<UpdateDeviceModelObj> deviceList) {
		this.deviceList = deviceList;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
}

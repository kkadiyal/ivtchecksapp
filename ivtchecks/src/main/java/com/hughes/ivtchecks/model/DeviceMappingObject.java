package com.hughes.ivtchecks.model;

public class DeviceMappingObject {

	private String deviceCd;
	private String deviceMapValue;
	private String deviceMapType;

	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}

	public String getDeviceMapValue() {
		return deviceMapValue;
	}

	public void setDeviceMapValue(String deviceMapValue) {
		this.deviceMapValue = deviceMapValue;
	}

	public String getDeviceMapType() {
		return deviceMapType;
	}

	public void setDeviceMapType(String deviceMapType) {
		this.deviceMapType = deviceMapType;
	}

}

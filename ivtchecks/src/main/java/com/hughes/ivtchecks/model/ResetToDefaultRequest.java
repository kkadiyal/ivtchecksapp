package com.hughes.ivtchecks.model;

public class ResetToDefaultRequest {

	private String companyId;
	private String deviceCd;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getDeviceCd() {
		return deviceCd;
	}

	public void setDeviceCd(String deviceCd) {
		this.deviceCd = deviceCd;
	}
}

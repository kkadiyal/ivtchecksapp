package com.hughes.ivtchecks.model;

public class CompanyIdChecksRequest {

	private String companyId;

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
}

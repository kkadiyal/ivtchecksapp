package com.hughes.ivtchecks.model;

import java.util.List;

public class ResponseObjForDeviceMapping {

	private String responseCode;
	private String responseDesc;
	private List<DeviceMappingObject> deviceCdMappingList;

	public List<DeviceMappingObject> getDeviceCdMappingList() {
		return deviceCdMappingList;
	}

	public void setDeviceCdMappingList(
			List<DeviceMappingObject> deviceCdMappingList) {
		this.deviceCdMappingList = deviceCdMappingList;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}
}

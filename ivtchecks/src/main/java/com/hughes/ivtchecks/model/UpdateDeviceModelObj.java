package com.hughes.ivtchecks.model;

public class UpdateDeviceModelObj {
	private String checkName;
	private String checkStatus;
	private String failureResult;

	public String getCheckName() {
		return checkName;
	}

	public void setCheckName(String checkName) {
		this.checkName = checkName;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getFailureResult() {
		return failureResult;
	}

	public void setFailureResult(String failureResult) {
		this.failureResult = failureResult;
	}
}

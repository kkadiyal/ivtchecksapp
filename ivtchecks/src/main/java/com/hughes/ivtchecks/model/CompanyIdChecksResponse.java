package com.hughes.ivtchecks.model;

import java.util.List;
import java.util.Map;

public class CompanyIdChecksResponse {
	private String responseCode;
	private String responseDesc;

	private Map<String, List<UiApiModel>> map;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public Map<String, List<UiApiModel>> getMap() {
		return map;
	}

	public void setMap(Map<String, List<UiApiModel>> map) {
		this.map = map;
	}

}

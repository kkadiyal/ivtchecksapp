package com.hughes.ivtchecks.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hughes.ivtchecks.model.CompaniesListResult;
import com.hughes.ivtchecks.model.CompanyIdChecksRequest;
import com.hughes.ivtchecks.model.CompanyIdChecksResponse;
import com.hughes.ivtchecks.model.DeviceCdMappingsRequest;
import com.hughes.ivtchecks.model.RequestCompanyObject;
import com.hughes.ivtchecks.model.ResetToDefaultRequest;
import com.hughes.ivtchecks.model.CommonResponseObj;
import com.hughes.ivtchecks.model.ResponseObjForDeviceMapping;
import com.hughes.ivtchecks.model.UpdateChecksRequest;
import com.hughes.ivtchecks.services.ICompaniesMasterListService;
import com.hughes.ivtchecks.services.ICompanyChecksService;
import com.hughes.ivtchecks.services.IDeviceTypeMappingService;

@CrossOrigin
@RestController
@RequestMapping("/IVTUIAPI")
public class IvtUiChecksController {

	@Autowired
	private ICompaniesMasterListService companiesMasterListService;

	@Autowired
	private IDeviceTypeMappingService deviceTypeMappingService;

	@Autowired
	private ICompanyChecksService companyChecksService;

	@GetMapping("/TestApp")
	public boolean authenticate() {
		return true;
	}

	@PostMapping("/GetCompanyDetails")
	public String getAllChecksForCompany(
			@RequestBody RequestCompanyObject companyObject) {
		if (companyObject == null || companyObject.getCompanyId() == null
				|| companyObject.getCompanyId().isEmpty()) {
			System.out.println("Company object or value is null or empty");
			return "Company object or value is null or empty";
		}
		System.out.println(companyObject.getCompanyId());

		return companyObject.getCompanyId();
	}

	@GetMapping("/GetAllNepEnabledCompnies")
	public CompaniesListResult getAllCustomersForNEPEnabled() {
		return companiesMasterListService
				.getMasterListOfComapaniesWithNEPEnabled();
	}

	@GetMapping("/GetDeviceTypeMappings")
	public ResponseObjForDeviceMapping getDeviceTypeMappingList() {
		return deviceTypeMappingService.getAllDeviceTypeMappingsMasterList();
	}

	@PostMapping("/GetMappingPerDeviceCd")
	public ResponseObjForDeviceMapping getMappingsPerDeviceCd(
			@RequestBody DeviceCdMappingsRequest request) {
		return deviceTypeMappingService
				.getAllDeviceTypeMappingsForDeviceCd(request);
	}

	@PostMapping("/LOOKUP")
	public CompanyIdChecksResponse fetchChecksConfiguredResult(
			@RequestBody CompanyIdChecksRequest request) {
		return companyChecksService.getConfiguredChecksForRequest(request);
	}

	@PostMapping("/RESETDEFAULT")
	public CommonResponseObj resetToDefaultForDeviceCd(
			@RequestBody ResetToDefaultRequest request) {
		return companyChecksService.resetToDefaultForDeviceCd(request);
	}

	@PostMapping("/UPDATE")
	public CommonResponseObj updateForDeviceCd(
			@RequestBody UpdateChecksRequest request) {
		return companyChecksService.updateForDeviceCdByCustomer(request);
	}
}
